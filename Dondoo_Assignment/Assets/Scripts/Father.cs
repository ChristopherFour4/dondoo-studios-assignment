﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Father class inherits from Human class
public class Father : Human {

    // Father shows his data on start
    void Start () {
        ShowMyData ();
    }

}