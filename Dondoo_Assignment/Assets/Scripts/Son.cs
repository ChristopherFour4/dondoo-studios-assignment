﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Son class inherits from Father class
public class Son : Father {

    // Son shows his data on start
    void Start () {
        base.ShowMyData ();
    }

}