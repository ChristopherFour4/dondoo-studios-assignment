﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour {

    // Camera's target to follow
    [SerializeField] Transform target;

    // Declaring the variables that the camera needs to move and follow the target
    [SerializeField] float hspeed = 5f;
    [SerializeField] float vspeed = 5f;
    [SerializeField] float moveSpeed = 5;
    public float camHeading;

    void Start () {
        // Cursor.visible = false;
        // Cursor.lockState = CursorLockMode.Locked;
    }

    void LateUpdate () {
        camHeading = transform.eulerAngles.y;

        // Controlling the camera with the mouse
        float h = hspeed * Input.GetAxisRaw ("Mouse X");
        float v = vspeed * Input.GetAxisRaw ("Mouse Y");

        //variables needed to coordinate look direction
        transform.Rotate (0, h, 0);
        transform.GetChild (0).Rotate (-v, 0, 0);

        // Follow the target
        if (target != null) {
            transform.position = Vector3.Lerp (transform.position, target.position, Time.deltaTime * moveSpeed);
        }

    }
}