﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleManager : MonoBehaviour {

    // Declaring as singleton
    public static VehicleManager instance;

    // private Vehicle Input Variables 
    float move_H;
    float move_V;
    float steerAngle;

    [Header ("Vehicle Driving Varables")]

    // Vehichle acceleration
    public float accelerationFactor = 1000f;

    // Slider for brake sensitivity
    [Range (1, 5)]
    [SerializeField] float brakeSensitivity = 2f;

    // Front wheel steering angles for turning;
    [SerializeField] float maxSteerAngle = 30f;

    // Vehicle Components
    // Wheel Meshes
    [SerializeField] Transform frontDriverWheel, frontPassWheel, backDriverWheel, backPassWheel;

    // Wheel Colliders
    [SerializeField] WheelCollider frontDriverWheelCol, frontPassWheelCol, backDriverWheelCol, backPassWheelCol;

    //  Vehicle Rigidboby 
    Rigidbody vRig;

    ///////////////////////////////////////////////////////////////////////////

    void Awake () {
        // Set this class as singleton
        instance = this;
    }

    void Start () {
        // Getting Vehicle Rigidbody componemt attached to VehicleBase
        vRig = GetComponent<Rigidbody> ();
    }

    // Getting Axis Input from User
    void GetInput () {
        move_H = Input.GetAxis ("Horizontal");
        move_V = Input.GetAxis ("Vertical");
    }

    // Vehicle steering via wheel direction using axis input for A/D keys
    void Steering () {
        steerAngle = maxSteerAngle * move_H;
        frontDriverWheelCol.steerAngle = steerAngle;
        frontPassWheelCol.steerAngle = steerAngle;
    }

    // Vehicle acceleration via axis input for W/S keys
    void Accelorate () {
        // Wheels rotation causing vehicle to move
        frontDriverWheelCol.motorTorque = move_V * accelerationFactor;
        frontPassWheelCol.motorTorque = move_V * accelerationFactor;
    }

    // Method for sending wheels into synch function;
    void WheelPositionController () {
        UpdateWheelPosition (frontDriverWheelCol, frontDriverWheel);
        UpdateWheelPosition (frontPassWheelCol, frontPassWheel);
        UpdateWheelPosition (backDriverWheelCol, backDriverWheel);
        UpdateWheelPosition (backPassWheelCol, backPassWheel);
    }

    // Upating and synching wheels with their wheel colliders
    void UpdateWheelPosition (WheelCollider wheelCol, Transform wheelTran) {

        Vector3 wheelPos = wheelTran.position;
        Quaternion wheelQuat = wheelTran.rotation;

        wheelCol.GetWorldPose (out wheelPos, out wheelQuat);
        wheelTran.position = wheelPos;
        wheelTran.rotation = wheelQuat;
    }

    // For resetting the vehicle if is falls over
    void VehicleReset () {
        transform.position = new Vector3 (transform.position.x, 0.9f, transform.position.z);
        transform.eulerAngles = new Vector3 (0f, transform.eulerAngles.y, 0f);
        Debug.Log ("Reset Completes");
    }

    // Vehicle functions and methods called in update
    void Update () {
        GetInput ();
        Steering ();
        Accelorate ();
        WheelPositionController ();

        // Reset Input
        if (Input.GetKeyDown (KeyCode.R)) {
            VehicleReset ();
        }

        // Brakes Input
        if (Input.GetKeyDown (KeyCode.Space)) {
            vRig.drag = brakeSensitivity;
        } else if (Input.GetKeyUp (KeyCode.Space)) {
            vRig.drag = 0;
        }

        // Boost Input
        if (Input.GetKeyDown (KeyCode.LeftShift)) {
            accelerationFactor = accelerationFactor * 2;
        } else if (Input.GetKeyUp (KeyCode.LeftShift)) {
            accelerationFactor = accelerationFactor / 2f;
        }
    }
}