﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class stores data and funtionality that will be inherited and used by the Father and Son classes respectively 
public class Human : MonoBehaviour {

    // Human traits 
    public string _name;
    public string _surname;
    public int _age;

    // This method will be used by Father and Son to log out their own information
    public void ShowMyData () {
        Debug.Log ($"My name is {_name}.");
        Debug.Log ($"My surname is {_surname}.");
        Debug.Log ($"I am {_age} years old.");

    }

}